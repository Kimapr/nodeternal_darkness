-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs
    = minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

function nodecore.get_depth_light()
	return 0
end

function nodecore.is_full_sun(pos)
	return minetest.get_node_light(pos, 0.5) >= 11
end

minetest.override_item("air", {
		visible_fluid_medium = "air",
		sunlight_propagates = false,
		groups = nodecore.underride({
				optic_transparent = 1,
				radiant_transparent = 1
			}, minetest.registered_nodes.air.groups)
	})

local replacements = {}
local function replace(old, new)
	replacements[minetest.get_content_id(old)] = minetest.get_content_id(new)
end

replace("nc_terrain:dirt_with_grass", "nc_terrain:dirt")
for k, v in pairs(minetest.registered_nodes) do
	if v.groups and v.groups.flora_sedges then
		replace(k, "air")
	end
	if v.groups and v.groups.flower_living then
		replace(k, v.flower_wilts_to)
	end
end

nodecore.register_mapgen_shared({
		label = "darkopocalypse",
		priority = -1000,
		func = function(minp, maxp, area, data)
			local ai = area.index
			for z = minp.z, maxp.z do
				for y = minp.y, maxp.y do
					local offs = ai(area, 0, y, z)
					for x = minp.x, maxp.x do
						local i = offs + x
						local dd = data[i]
						if replacements[dd] then
							data[i] = replacements[dd]
						end
					end
				end
			end
		end
	})

local oldsound = nodecore.sound_play
function nodecore.sound_play(name, spec, ...)
	if name == "nc_envsound_drip" then
		local pos = spec.pos
		for _ = 1, 16 do
			pos.y = pos.y + 1
			local nn = minetest.get_node(pos).name
			if nn == "ignore" then
				break
			elseif not nodecore.air_equivalent(nn) then
				return oldsound(name, spec, ...)
			end
		end
		name = "nodeternal_darkness_creepy"
	end
	return oldsound(name, spec, ...)
end